import busStopService, { BusStop } from "@/services/busStopService";
import { createStore } from "vuex";

export type MappedBusStop<T extends BusStop> = {
  [key in keyof T]: key extends "time" ? string[] : T[key];
};

type State = {
  transportInfo: Record<number, Record<string, MappedBusStop<BusStop>>>;
  isLoading: boolean;
};

export default createStore<State>({
  state: {
    transportInfo: {},
    isLoading: true,
  },
  getters: {
    busLines(state): string[] {
      return Object.keys(state.transportInfo).sort();
    },
    busStopsByBusLine: (state) => (busLine: number) => {
      return busLine ? Object.values(state.transportInfo[busLine]) : [];
    },
    busStops(state: State): MappedBusStop<BusStop>[] {
      return Object.entries(state.transportInfo)
        .map(([_, busLineInfo]) => Object.values(busLineInfo))
        .flat()
        .reduce(
          (acc: MappedBusStop<BusStop>[], curr) =>
            !acc.find(el => el.stop === curr.stop) ? [...acc, curr] : acc,
          []
        );
    },
  },
  mutations: {
    initializeTransportSystemInfo(state, payload) {
      state.transportInfo = payload;
    },
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
  },
  actions: {
    async loadBusStops({ commit }) {
      commit("setIsLoading", true);

      try {
        const totalBusStopsInfo = await busStopService.getBusStopsInfo();
        const groupedBusStopInfo = totalBusStopsInfo.reduce(
          (acc: State["transportInfo"], bs: BusStop) => {
            const { line, stop, time } = bs;
            const busLine = acc[line];

            if (busLine) {
              if (busLine[stop]) {
                busLine[stop] = { ...bs, time: [...busLine[stop].time, time] };
              } else {
                busLine[stop] = { ...bs, time: [time] };
              }
            } else {
              acc[line] = { [bs.stop]: { ...bs, time: [time] } };
            }

            return acc;
          },
          {}
        );

        commit("initializeTransportSystemInfo", groupedBusStopInfo);
      } finally {
        commit("setIsLoading", false);
      }
    },
  },
});
