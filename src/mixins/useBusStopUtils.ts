import { MappedBusStop } from '@/store/index';
import { BusStop } from '@/services/busStopService';
import { Order } from '@/shared/types';

type BSType = MappedBusStop<BusStop>;

export default {
    methods: {
        renderCallback(item: BSType) {
            return item.stop;
        },
        filterCallback(filterValue: string) {
            return function (val: unknown): boolean {
                return (val as BSType).stop.toLowerCase().includes(filterValue.toLowerCase())
            }
        },
        sortCallback(order: Order) {
            return function (i1: BSType, i2: BSType) {
                switch (order) {
                    case 'asc': {
                        return i1.order - i2.order;
                    }
                    case 'desc': {
                        return i2.order - i1.order;
                    }
                    case 'none':
                    default: {
                        return 0;
                    }
                }
            };
        }
    },
}