import { api } from "@/api";

export type BusStop = {
  line: number;
  stop: string;
  order: number;
  time: string;
};

export default {
  async getBusStopsInfo() {
    const response = await api.get("/stops");
    return response.data;
  },
};
