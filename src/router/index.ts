import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import BusLine from '@/layout/BusLine.vue';
import Stops from '@/layout/Stops.vue';

const routes: Array<RouteRecordRaw> = [
  { path: '/', component: BusLine }, 
  { path: '/stops', component: Stops }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
